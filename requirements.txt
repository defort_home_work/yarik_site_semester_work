asgiref==3.8.1
beautifulsoup4==4.12.3
certifi==2024.2.2
charset-normalizer==3.3.2
Django==5.0.4
environs==11.0.0
idna==3.6
marshmallow==3.21.1
packaging==24.0
psycopg2==2.9.9
python-dateutil==2.9.0.post0
python-dotenv==1.0.1
requests==2.31.0
six==1.16.0
soupsieve==2.5
sqlparse==0.4.4
tzdata==2024.1
urllib3==2.2.1
