from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import AuthenticationForm
from web_main_app.forms import SignUpForm
from web_main_app.services import (
    fetch_mortgage_offers,
    delete_and_create_mortgages,
    get_mortgages,
)


def auth_required(function):
    def wrapper(request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect("login")
        return function(request, *args, **kwargs)

    return wrapper


def home_view(request):
    mortgages = get_mortgages(3)
    context = {
        "is_main_page": True,
        "mortgages": mortgages,
    }
    return render(request, 'web_main_app/web/main.html', context=context)


def suggestions_view(request):
    mortgages = get_mortgages()
    context = {
        "is_suggestions_page": True,
        "mortgages": mortgages,
    }
    return render(request, 'web_main_app/web/suggestions.html', context=context)


def signup_view(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('account')
    else:
        form = SignUpForm()
    return render(request, 'web_main_app/auth/sign_up.html', {'form': form})


def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('account')
            else:
                return render(request, 'web_main_app/auth/login.html', {'form': form, 'error': 'Неверное имя пользователя или пароль'})
    else:
        form = AuthenticationForm()
    return render(request, 'web_main_app/auth/login.html', {'form': form})


def logout_view(request):
    logout(request)
    return redirect('login')


def calculator_view(request):
    mortgages = get_mortgages()
    context = {
        "is_calculator_page": True,
        "mortgages": mortgages[:min(len(mortgages), 3)],
    }
    return render(request, 'web_main_app/web/calculator.html', context=context)


def get_new_mortgages(request):
    mortgages = fetch_mortgage_offers()
    delete_and_create_mortgages(mortgages)
    return redirect('home')


@auth_required
def account_view(request):
    context = {
        "is_account_page": True,
    }
    return render(request, 'web_main_app/web/account.html', context=context)
