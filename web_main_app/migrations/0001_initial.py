# Generated by Django 5.0.4 on 2024-04-13 19:50

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Mortgage',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bank_name', models.CharField(max_length=100, verbose_name='Название банка')),
                ('link', models.CharField(max_length=100, verbose_name='Ссылка на предложение')),
                ('percent', models.CharField(max_length=100, verbose_name='Процентная ставка')),
                ('payment', models.CharField(max_length=100, verbose_name='Ежемесячный платеж')),
                ('amount', models.CharField(max_length=100, verbose_name='Сумма кредита')),
                ('loan_rate', models.CharField(max_length=100, verbose_name='Ставка по кредиту')),
            ],
            options={
                'verbose_name': 'Ипотека',
                'verbose_name_plural': 'Ипотеки',
            },
        ),
    ]
