import requests
from bs4 import BeautifulSoup
from web_main_app.models import Mortgage


BANK_TO_PAGE = {
    "ALL": "https://www.banki.ru/products/hypothec/?source=main_menu_hypothec",
    "Сбербанк": "http://www.sberbank.ru/ru/person/credits/homenew",
    "ВТБ": "https://www.vtb.ru/personal/ipoteka/",
    "Росбанк": "https://www.banki.ru/products/hypothec/?source=main_menu_hypothec",
    "Альфа-Банк": "https://www.banki.ru/products/hypothec/?source=main_menu_hypothec",
    "ПСБ": "https://www.banki.ru/products/hypothec/?source=main_menu_hypothec",
    "РНКБ": "https://www.banki.ru/products/hypothec/?source=main_menu_hypothec",
}


def fetch_mortgage_offers(url: str = "https://www.banki.ru/products/hypothec/?source=main_menu_hypothec"):
    response = requests.get(url)
    if response.status_code != 200:
        print("Ошибка получения страницы")
        return None

    page_content = response.content
    soup = BeautifulSoup(page_content, 'html.parser')

    # Найти все карточки с предложениями
    offers = soup.find_all('tbody')[0]
    trs = offers.find_all("tr")
    offer_data = []
    for tr in trs:
        # Название банка
        tds = tr.find_all('td')
        if not tds or tds[1].text not in ["Сбербанк", "ВТБ", "Росбанк", "Альфа-Банк", "ПСБ", "РНКБ"]:
            continue

        bank_name = tds[1].text
        percent = tds[2].text
        payment = tds[3].text
        # link = tr.find_all('a')[0]["href"]
        link = BANK_TO_PAGE[bank_name]
        additional_info = tds[4].find_all("th")
        amount = additional_info[-2].text
        loan_rate = additional_info[-1].text
        if "-" not in amount:
            loan_rate = f"{loan_rate} - {amount}"
            amount = additional_info[-3].text
        if "-" not in amount:
            amount = additional_info[-4].text

        offer_data.append({
            "bank_name": bank_name,
            "link": link,
            "percent": percent,
            "payment": payment,
            "amount": amount,
            "loan_rate": loan_rate,
        })

    return offer_data


def delete_all_mortgages():
    Mortgage.objects.all().delete()


def create_mortgages(offer_data):
    new_mortgages = [
        Mortgage(bank_name=offer["bank_name"],
                 link=offer["link"],
                 percent=offer["percent"],
                 payment=offer["payment"],
                 amount=offer["amount"],
                 loan_rate=offer["loan_rate"])
        for offer in offer_data
    ]

    Mortgage.objects.bulk_create(new_mortgages)


def delete_and_create_mortgages(offer_data):
    delete_all_mortgages()
    create_mortgages(offer_data)


def get_mortgages(amount: int | None = None):
    mortgages = Mortgage.objects.all()
    if amount is not None:
        return mortgages[:min(amount, len(mortgages))]

    return mortgages
