from django.db import models


class Mortgage(models.Model):
    bank_name = models.CharField(max_length=100, verbose_name="Название банка")
    link = models.CharField(max_length=100, verbose_name="Ссылка на предложение")
    percent = models.CharField(max_length=100, verbose_name="Процентная ставка")
    payment = models.CharField(max_length=100, verbose_name="Ежемесячный платеж")
    amount = models.CharField(max_length=100, verbose_name="Сумма кредита")
    loan_rate = models.CharField(max_length=100, verbose_name="Ставка по кредиту")

    def __str__(self):
        return f"{self.bank_name} - {self.percent}%"

    class Meta:
        verbose_name = "Ипотека"
        verbose_name_plural = "Ипотеки"
