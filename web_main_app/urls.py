from django.urls import path
from web_main_app.views import (
    signup_view,
    login_view,
    logout_view,
    home_view,
    account_view,
    calculator_view,
    get_new_mortgages,
    suggestions_view,
)

urlpatterns = [
    path('signup/', signup_view, name='signup'),
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('', home_view, name='home'),
    path('calculator/', calculator_view, name='calculator'),
    path('account/', account_view, name='account'),
    path('suggestions/', suggestions_view, name='suggestions'),
    path('get_new_mortgages/', get_new_mortgages, name='get_new_mortgages'),
]
