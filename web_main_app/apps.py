from django.apps import AppConfig


class WebMainAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'web_main_app'
